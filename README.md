# GitLab migration tracking repository

This project is used for:

 * tracking issues with the migration

 * maintaining the the Wiki page name mapping (`wiki-mapping.json`)
   used to generate redirects from Trac URLs to their corresponding GitLab
   pages.

